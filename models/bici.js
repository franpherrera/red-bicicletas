var Bici=function(id, color, modelo, ubicacion){
  this.id=id;
  this.color=color;
  this.modelo=modelo;
  this.ubicacion=ubicacion;
}

Bici.prototype.toString=function(){
  return "id: "+this.id+ " | color: " +this.color;
}
Bici.allBicis=[];

Bici.add=function(aBici){
  Bici.allBicis.push(aBici);
}

Bici.findbyId = function(aBiciId){
  var aBici = Bici.allBicis.find(x =>x.id == aBiciId);
  if (aBici)
    return aBici;
  else
    throw new Error('No existe bicicleta con ese Id');
}

Bici.removebyId = function(aBiciId){
  for (var i=0; i<Bici.allBicis.length; i++){
    if(Bici.allBicis[i].id == aBiciId){
      Bici.allBicis.splice(i, 1);
      break;
    }
  }
}



var a=new Bici(1,'rojo','urbana',[-33.44048, -70.63150]);
var b=new Bici(2,'amarilla','paseo',[-33.43738, -70.63433]);

Bici.add(a);
Bici.add(b);


module.exports=Bici;
