var express = require('express');
var router = express.Router();
var biciController = require('../../controllers/api/biciControllerAPI');

router.get('/', biciController.bici_list);
router.post('/create', biciController.bici_create);
router.delete('/delete', biciController.bici_delete);
module.exports = router;
