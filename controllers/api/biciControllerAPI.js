var Bici = require('../../models/bici');

exports.bici_list = function(req, res){
  res.status(200).json({
    bicis: Bici.allBicis
  });
}

exports.bici_create = function(req, res){
  var bici = new Bici(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion=[req.body.lat, req.body.lng];

  Bici.add(bici);
  res.status(200).json({
    bicis: bici
  });
}

exports.bici_delete = function(req, res){
  Bici.removebyId(req.body.id);
  res.status(204).send();
}
