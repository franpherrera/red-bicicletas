var Bici=require('../models/bici');

exports.bici_list=function(req, res){
  res.render('bicis/index',{bicis:Bici.allBicis});
}

exports.bici_create_get=function(req, res){
  res.render('bicis/create');
}

exports.bici_create_post=function(req, res){
  var bici=new Bici(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion=[req.body.lat, req.body.lng];
  Bici.add(bici);
  res.redirect('/bicis');
}

exports.bici_delete_post=function(req, res){
  Bici.removebyId(req.body.id);
  res.redirect('/bicis');
}

exports.bici_update_get=function(req, res){
  var bici = Bici.findbyId(req.params.id);
  res.render('bicis/update', {bici});
}

exports.bici_update_post=function(req, res){
  var bici = Bici.findbyId(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion=[req.body.lat, req.body.lng];

  res.redirect('/bicis');
}
