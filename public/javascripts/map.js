var map=L.map('main_map').setView([-33.4398,-70.6347], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
}).addTo(map);

//var bici1_marker = L.marker([-33.43756, -70.63326]).addTo(map);
//var bici2_marker = L.marker([-33.44048, -70.63150]).addTo(map);
//var bici3_marker = L.marker([-33.43738, -70.63433]).addTo(map);

$.ajax({
  dataType: "json",
  url: "api/bicis",
  success: function(result){
    console.log(result);
    result.bicis.forEach(function(bici){
        L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
    });
  }
})
